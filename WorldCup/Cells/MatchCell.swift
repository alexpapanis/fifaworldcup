//
//  MatchCell.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import UIKit

class MatchCell: UITableViewCell {

    var utils = Utils()
    
    @IBOutlet weak var vwBackground: UIView!
    @IBOutlet weak var lbStadium: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbHomeTeam: UILabel!
    @IBOutlet weak var lbHomeGoals: UILabel!
    @IBOutlet weak var lbAwayGoals: UILabel!
    @IBOutlet weak var lbAwayTeam: UILabel!
    
    var match: Match! {
        didSet {
            updateUI()
        }
    }
    
    func updateUI(){
        
        lbDate.text = utils.formatDate(originalDate: match.date!)
        if let goalsHomeTeam = match.home_result{
            lbHomeGoals.text = "\(goalsHomeTeam)"
        } else {
            lbHomeGoals.text = ""
        }
        
        if let goalsAwayTeam = match.away_result{
            lbAwayGoals.text = "\(goalsAwayTeam)"
        } else {
            lbAwayGoals.text = ""
        }
        
        if let matchHomeTeam = match?.home_team {
            
            let homeTeam = utils.getTeamById(id: matchHomeTeam)
            lbHomeTeam.text = homeTeam.fifaCode
            
            //check if the match already happened
            if let homeResult = match?.home_result {
                if let awayResult = match?.away_result {
                    
                    if homeResult > awayResult {
                        //Home team is the winner
                        
                        lbHomeTeam.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                        lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbHomeGoals.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                        lbAwayGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbHomeTeam.textColor = UIColor.blue
                        lbAwayTeam.textColor = UIColor.red
                        lbHomeGoals.textColor = UIColor.blue
                        lbAwayGoals.textColor = UIColor.red
                        
                    } else if homeResult < awayResult {
                        //Away team is the winner
                        
                        lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbAwayTeam.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                        lbHomeGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbAwayGoals.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                        lbHomeTeam.textColor = UIColor.red
                        lbAwayTeam.textColor = UIColor.blue
                        lbHomeGoals.textColor = UIColor.red
                        lbAwayGoals.textColor = UIColor.blue
                    } else {
                        //it is a draw
                        
                        lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbHomeGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbAwayGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
                        lbHomeTeam.textColor = UIColor.orange
                        lbAwayTeam.textColor = UIColor.orange
                        lbHomeGoals.textColor = UIColor.orange
                        lbAwayGoals.textColor = UIColor.orange
                    }
                }
            } else {
                //the match didn't happen yet
                
                lbHomeTeam.text = "Undefined"
                lbAwayTeam.text = "Undefined"
                lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                lbHomeGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
                lbAwayGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
                lbHomeTeam.textColor = UIColor.black
                lbAwayTeam.textColor = UIColor.black
                lbHomeGoals.textColor = UIColor.black
                lbAwayGoals.textColor = UIColor.black
            }
            
        } else {
            // the teams are still not defined
            
            lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
            lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
            lbHomeGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
            lbAwayGoals.font = UIFont.init(name: "HelveticaNeue", size: 18)
        }
        
    }
}
