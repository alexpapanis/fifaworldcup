//
//  Team.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation

public class Team {
    var id: Int?
    var name: String?
    var fifaCode: String?
    var iso2: String?
    var flag: String?
    var emoji: String?
    var emojiString: String?
}
