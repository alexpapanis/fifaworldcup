//
//  Stadium.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation

public class Stadium {
    var id: Int = 0
    var name: String?
    var city: String?
    var lat: Double = 0
    var lng: Double = 0
    var image: String?
    
}
