//
//  TvChannels.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation

public class TvChannel {
    
    var id: Int = 0
    var name: String?
    var icon: String?
    var country: String?
    var iso2: String?
    var lang: [String] = []
    
}
