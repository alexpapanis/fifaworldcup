//
//  Match.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation

public class Match {
    var name: Int = 0
    var type: String?
    var home_team: Int?
    var away_team: Int?
    var home_knockout_team: String?
    var away_knockout_team: String?
    var home_result: Int?
    var away_result: Int?
    var home_penalty: Int?
    var away_penalty: Int?
    var date: String?
    var stadium: Int = 0
    var channels: [Int] = []
    var finished: Bool?
    var matchday: Int = 0
}
