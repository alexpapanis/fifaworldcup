//
//  Knockout.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation

public class Knockout {
    var name: String?
    var matches: [Match] = []
}
