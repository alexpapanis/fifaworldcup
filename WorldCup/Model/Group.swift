//
//  Group.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation

public class Group {
    var name: String?
    var winner: String?
    var runnerup: String?
    var matches: [Match] = []
}


