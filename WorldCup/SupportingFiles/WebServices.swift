//
//  WebServices.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class WebServices {
    
    var utils = Utils()
    
    func updateWorldCupJson(completion: @escaping (_ sucesso: Bool) -> Void){
        
        Alamofire.request(URL(string: Defines.FIFA_WORLD_CUP_JSON)!).responseJSON { response in
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    print("updateWorldCupJson sucess")
                    if let result = response.result.value {
                        let json = JSON(result)
                        
                        self.utils.saveAtividadesToJsonFile(json: json)
                    }
                    completion(true)
                default:
                    print("error with response status: \(status)")
                    completion(false)
                }
            }
        }
    }
    
    
    
}
