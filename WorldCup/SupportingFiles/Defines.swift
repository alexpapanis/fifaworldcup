//
//  Defines.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation

class Defines{
    
    static let FIFA_WORLD_CUP_JSON = "https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json"
    
}
