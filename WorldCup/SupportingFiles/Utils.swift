//
//  Utils.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import Foundation
import SwiftyJSON

class Utils {
    
    let worldCupGroups = ["a","b","c","d","e","f","g","h"]
    let worldCupKnockouts = ["round_16","round_8","round_4","round_2_loser","round_2"]
    
    var stadiums: [Stadium] = []
    var tvChannels: [TvChannel] = []
    var teams: [Team] = []
    var groups: [Group] = []
    var knockouts: [Knockout] = []
    
    func saveAtividadesToJsonFile(json: JSON) {
        
        if let data = try? json.rawData() {
            
            do {
                
                let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    .appendingPathComponent("fifa_world_cup.json")
                
                try data.write(to: fileUrl, options: .atomicWrite)
                print("data updated")
            }catch {
                print("error when writing json document")
            }
        }
    }
    
    func retrieveJsonData(completion: @escaping (_ stadiums: [Stadium], _ tvChannels: [TvChannel], _ teams: [Team], _ groups: [Group], _ knockouts: [Knockout]) -> Void){
        
        stadiums = []
        tvChannels = []
        teams = []
        groups = []
        knockouts = []
        
        do{
            let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                .appendingPathComponent("fifa_world_cup.json") // Your json file name
            
            if let data = try? Data(contentsOf: fileUrl) {
                let jsonObj = try JSON(data: data)
                
                if jsonObj != JSON.null {
                    
                    if let stadiumsJson = jsonObj["stadiums"].array {
                        for stadiumJson in stadiumsJson {
                            let stadium = Stadium()
                            stadium.id = stadiumJson["id"].int!
                            stadium.name = stadiumJson["name"].string
                            stadium.city = stadiumJson["city"].string
                            stadium.lat = stadiumJson["lat"].double!
                            stadium.lng = stadiumJson["lng"].double!
                            stadium.image = stadiumJson["image"].string
                            stadiums.append(stadium)
                        }
                    }
                    
                    if let tvChannelsJson = jsonObj["tvchannels"].array {
                        for tvChannelJson in tvChannelsJson {
                            let tvChannel = TvChannel()
                            tvChannel.id = tvChannelJson["id"].int!
                            tvChannel.name = tvChannelJson["name"].string
                            tvChannel.icon = tvChannelJson["icon"].string
                            tvChannel.country = tvChannelJson["country"].string
                            tvChannel.iso2 = tvChannelJson["iso2"].string
                            tvChannel.lang = tvChannelJson["lang"].arrayObject as! [String]
                            tvChannels.append(tvChannel)
                        }
                    }
                    
                    if let teamsJson = jsonObj["teams"].array {
                        for teamJson in teamsJson {
                            let team = Team()
                            team.id = teamJson["id"].int!
                            team.name = teamJson["name"].string
                            team.fifaCode = teamJson["fifaCode"].string
                            team.iso2 = teamJson["iso2"].string
                            team.flag = teamJson["flag"].string
                            team.emoji = teamJson["emoji"].string
                            team.emojiString = teamJson["emojiString"].string
                            teams.append(team)
                        }
                    }
                    
                    let groupsJson = jsonObj["groups"]
                    
                    for worldCupGroup in worldCupGroups {
                        let group = Group()
                        group.name = groupsJson[worldCupGroup]["name"].string
                        group.winner = groupsJson[worldCupGroup]["winner"].string
                        group.runnerup = groupsJson[worldCupGroup]["runnerup"].string
                        
                        if let matchesJson = groupsJson[worldCupGroup]["matches"].array {
                            for matchJson in matchesJson{
                                let match = Match()
                                match.name = matchJson["name"].int!
                                match.type = matchJson["type"].string
                                match.home_team = matchJson["home_team"].int!
                                match.away_team = matchJson["away_team"].int!
                                if let homeResult = matchJson["home_result"].int {
                                    match.home_result = homeResult
                                }
                                if let awayResult = matchJson["away_result"].int {
                                    match.away_result = awayResult
                                }
                                match.date = matchJson["date"].string
                                match.stadium = matchJson["stadium"].int!
                                match.channels = matchJson["channels"].arrayObject as! [Int]
                                match.finished = matchJson["finished"].boolValue
                                match.matchday = matchJson["matchday"].int!
                                group.matches.append(match)
                            }
                        }
                        groups.append(group)
                    }
                    
                    let knockoutsJson = jsonObj["knockout"]
                    
                    for worldCupKnockout in worldCupKnockouts {
                        let knockout = Knockout()
                        knockout.name = knockoutsJson[worldCupKnockout]["name"].string
                        
                        if let matchesJson = knockoutsJson[worldCupKnockout]["matches"].array {
                            for matchJson in matchesJson{
                                let match = Match()
                                match.name = matchJson["name"].int!
                                match.type = matchJson["type"].string
                                if let homeTeam = matchJson["home_team"].int {
                                    match.home_team = homeTeam
                                }
                                if let awayTeam = matchJson["away_team"].int {
                                    match.away_team = awayTeam
                                }
                                if let homeResult = matchJson["home_result"].int {
                                    match.home_result = homeResult
                                }
                                if let awayResult = matchJson["away_result"].int {
                                    match.away_result = awayResult
                                }
                                match.home_knockout_team = matchJson["home_team"].string
                                match.away_knockout_team = matchJson["away_team"].string
                                match.date = matchJson["date"].string
                                match.stadium = matchJson["stadium"].int!
                                match.channels = matchJson["channels"].arrayObject as! [Int]
                                match.finished = matchJson["finished"].boolValue
                                match.matchday = matchJson["matchday"].int!
                                knockout.matches.append(match)
                            }
                        }
                        knockouts.append(knockout)
                    }
                    completion(stadiums, tvChannels, teams, groups, knockouts)
                    
                } else {
                    print("Could not get json from file, make sure that file contains valid json.")
                }
            }
            
        } catch {
            print("erro ao ler JSON")
        }
    }
    
    func getStadiumById(id: Int)-> Stadium {
        
        do{
            let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                .appendingPathComponent("fifa_world_cup.json") // Your json file name
            
            if let data = try? Data(contentsOf: fileUrl) {
                let jsonObj = try JSON(data: data)
                
                if jsonObj != JSON.null {
                    
                    if let stadiumsJson = jsonObj["stadiums"].array {
                        for stadiumJson in stadiumsJson {
                            if stadiumJson["id"].int! == id {
                                let stadium = Stadium()
                                stadium.id = stadiumJson["id"].int!
                                stadium.name = stadiumJson["name"].string
                                stadium.city = stadiumJson["city"].string
                                stadium.lat = stadiumJson["lat"].double!
                                stadium.lng = stadiumJson["lng"].double!
                                stadium.image = stadiumJson["image"].string
                                return stadium
                            }
                        }
                    }
                }
            }
            
        } catch {
            print("erro ao ler JSON")
        }
        return Stadium()
        
    }
    
    func getTeamById(id: Int)-> Team {
        
        do{
            let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                .appendingPathComponent("fifa_world_cup.json") // Your json file name
            
            if let data = try? Data(contentsOf: fileUrl) {
                let jsonObj = try JSON(data: data)
                
                if jsonObj != JSON.null {
                    if let teamsJson = jsonObj["teams"].array {
                        for teamJson in teamsJson {
                            if teamJson["id"].int! == id {
                                let team = Team()
                                if let teamId = teamJson["id"].int {
                                    team.id = teamId
                                }
                                team.name = teamJson["name"].string
                                team.fifaCode = teamJson["fifaCode"].string
                                team.iso2 = teamJson["iso2"].string
                                team.flag = teamJson["flag"].string
                                team.emoji = teamJson["emoji"].string
                                team.emojiString = teamJson["emojiString"].string
                                return team
                            }
                        }
                    }
                }
            }
            
        } catch {
            print("erro ao ler JSON")
        }
        return Team()
        
    }
    
    func getGroupByName(text: String) -> [Group] {
        var filteredGroups: [Group] = []
        
        filteredGroups = groups.filter({$0.name == text})
        
        return filteredGroups
    }
    
    func getKnockoutByName(text: String) -> [Knockout] {
        var filteredKnockouts: [Knockout] = []
        
        filteredKnockouts = knockouts.filter({$0.name == text})
        
        return filteredKnockouts
    }
    
    func formatDate(originalDate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let formattedDate = dateFormatter.date(from: originalDate)
        
        dateFormatter.timeZone = TimeZone(abbreviation: TimeZone.current.abbreviation()!)
        dateFormatter.dateFormat = "MM/dd/yy HH:mm"
        return dateFormatter.string(from: formattedDate!)
    }
    
}
