//
//  FakeSplashViewController.swift
//  FifaWorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class FakeSplashViewController: UIViewController {

    var ws = WebServices()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ws.updateWorldCupJson(completion: { success in
            if(success) {
                print("json updated")
                self.performSegue(withIdentifier: "showHome", sender: nil)
            }
            
        })
    }


}
