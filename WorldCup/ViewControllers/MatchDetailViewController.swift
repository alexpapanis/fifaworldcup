//
//  MatchDetailViewController.swift
//  WorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import UIKit
import SDWebImage

class MatchDetailViewController: UIViewController {

    var match: Match?
    fileprivate var utils = Utils()
    
    @IBOutlet weak var lbMatchNumber: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbCity: UILabel!
    @IBOutlet weak var lbStadium: UILabel!
    @IBOutlet weak var imgStadium: UIImageView!
    
    @IBOutlet weak var lbHomeTeam: UILabel!
    @IBOutlet weak var lbAwayTeam: UILabel!
    @IBOutlet weak var lbGoalsHomeTeam: UILabel!
    @IBOutlet weak var lbGoalsAwayTeam: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Functions
    func updateUI(){
        let stadium = utils.getStadiumById(id: (match?.stadium)!)
        
        lbMatchNumber.text = "Match #\((match?.name)!)"
        lbDate.text = utils.formatDate(originalDate: (match?.date)!)
        lbCity.text = "City: " + stadium.city!
        lbStadium.text = "Stadium: " + stadium.name!
        imgStadium.sd_setImage(with: URL(string: stadium.image!), completed: nil)
        
        if let homeResult = match?.home_result {
            lbGoalsHomeTeam.text = "\(homeResult)"
        } else {
            lbGoalsHomeTeam.text = ""
        }
        
        if let awayResult = match?.away_result {
            lbGoalsAwayTeam.text = "\(awayResult)"
        } else {
            lbGoalsAwayTeam.text = ""
        }
        
        if match?.type == "qualified" {
            lbHomeTeam.text = match?.home_knockout_team
            lbAwayTeam.text = match?.away_knockout_team
        } else {
            if let matchHomeTeam = match?.home_team {
                
                let homeTeam = utils.getTeamById(id: matchHomeTeam)
                lbHomeTeam.text = homeTeam.fifaCode
                
                //check if the match already happened
                if let homeResult = match?.home_result {
                    if let awayResult = match?.away_result {
                        
                        if homeResult > awayResult {
                            //Home team is the winner
                            
                            lbHomeTeam.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                            lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbGoalsHomeTeam.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                            lbGoalsAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbHomeTeam.textColor = UIColor.blue
                            lbAwayTeam.textColor = UIColor.red
                            lbGoalsHomeTeam.textColor = UIColor.blue
                            lbGoalsAwayTeam.textColor = UIColor.red
                            
                        } else if homeResult < awayResult {
                            //Away team is the winner
                            
                            lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbAwayTeam.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                            lbGoalsHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbGoalsAwayTeam.font = UIFont.init(name: "HelveticaNeue-Bold", size: 18)
                            lbHomeTeam.textColor = UIColor.red
                            lbAwayTeam.textColor = UIColor.blue
                            lbGoalsHomeTeam.textColor = UIColor.red
                            lbGoalsAwayTeam.textColor = UIColor.blue
                        } else {
                            //it is a draw
                            
                            lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbGoalsHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbGoalsAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                            lbHomeTeam.textColor = UIColor.orange
                            lbAwayTeam.textColor = UIColor.orange
                            lbGoalsHomeTeam.textColor = UIColor.orange
                            lbGoalsAwayTeam.textColor = UIColor.orange
                        }
                    }
                } else {
                    lbHomeTeam.text = "Undefined"
                    lbAwayTeam.text = "Undefined"
                    lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                    lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                    lbGoalsHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                    lbGoalsAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                    lbHomeTeam.textColor = UIColor.black
                    lbAwayTeam.textColor = UIColor.black
                    lbGoalsHomeTeam.textColor = UIColor.black
                    lbGoalsAwayTeam.textColor = UIColor.black
                }
                
            } else {
                // the teams are still not defined
                
                lbHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                lbAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                lbGoalsHomeTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
                lbGoalsAwayTeam.font = UIFont.init(name: "HelveticaNeue", size: 18)
            }
//            else {
//                lbHomeTeam.text = "Undefined"
//            }
            
//            if let matchAwayTeam = match?.away_team {
//                let awayTeam = utils.getTeamById(id: matchAwayTeam)
//                lbAwayTeam.text = awayTeam.fifaCode
//            } else {
//                lbAwayTeam.text = "Undefined"
//            }
        }
        
    }


}
