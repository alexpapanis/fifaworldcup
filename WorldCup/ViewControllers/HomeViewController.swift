//
//  HomeViewController.swift
//  WorldCup
//
//  Created by Alexandre Papanis on 24/06/2018.
//  Copyright © 2018 Papanis. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import RxSwift
import RxCocoa

class HomeViewController: UIViewController{
    
    var ws = WebServices()
    var utils = Utils()
    var dataJson: JSON = ""
    
    var isGroup: Bool = true
    
    var searchTerm = BehaviorRelay<String>(value: "")
    var disposeBag = DisposeBag()
    
    var filteredGroups: [Group] = []
    var filteredKnockouts: [Knockout] = []
    var btGroups: [UIButton] = []
    var btGroupSelected: UIButton?
    var groupNames = ["Group A", "Group B", "Group C", "Group D", "Group E", "Group F", "Group G", "Group H"]
    var groupShortNames = ["A", "B", "C", "D", "E", "F", "G", "H"]
    
    @IBOutlet weak var vwGroups: UIView!
    @IBOutlet weak var heightVwGroups: NSLayoutConstraint!
    
    @IBOutlet weak var btGroupsButton: UIButton!
    @IBOutlet weak var btKnockoutsButton: UIButton!
    
    
    @IBOutlet weak var btGroupA: UIButton!
    @IBOutlet weak var btGroupB: UIButton!
    @IBOutlet weak var btGroupC: UIButton!
    @IBOutlet weak var btGroupD: UIButton!
    @IBOutlet weak var btGroupE: UIButton!
    @IBOutlet weak var btGroupF: UIButton!
    @IBOutlet weak var btGroupG: UIButton!
    @IBOutlet weak var btGroupH: UIButton!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func changeGroups(_ sender: UIButton) {
        
        for btGroup in btGroups {
            if btGroup == btGroupSelected {
                deselectButton(button: btGroup)
            } else if btGroup == sender {
                selectButton(button: btGroup)
            } else {
                deselectButton(button: btGroup)
            }
        }
        
        if btGroupSelected == sender {
            btGroupSelected = UIButton()
            filteredGroups = utils.groups
        } else {
            filteredGroups = utils.getGroupByName(text: groupNames[sender.tag])
            btGroupSelected = sender
        }
        
        tableView.reloadData()
        
    }
    
    func deselectButton(button: UIButton){
        
        button.backgroundColor = UIColor.init(red: 158/255, green: 208/255, blue: 207/255, alpha: 1.0)
        button.setTitleColor(UIColor.black, for: .normal)
        
    }
    
    func selectButton(button: UIButton){
        
        button.backgroundColor = UIColor.black
        button.setTitleColor(UIColor.init(red: 158/255, green: 208/255, blue: 207/255, alpha: 1.0), for: .normal)
        
    }
    
    @IBAction func pressedGroups(_ sender: UIButton) {
        if !isGroup {
            heightVwGroups.constant = 40
            isGroup = true
            updateUI()
            selectButton(button: btGroupsButton)
            deselectButton(button: btKnockoutsButton)
            for btGroup in btGroups {
                btGroup.setTitle(groupShortNames[btGroup.tag], for: .normal)
            }
        }
    }
    
    @IBAction func pressedKnockouts(_ sender: UIButton) {
        if isGroup {
            heightVwGroups.constant = 0
            isGroup = false
            updateUI()
            selectButton(button: btKnockoutsButton)
            deselectButton(button: btGroupsButton)
            for btGroup in btGroups {
                btGroup.setTitle("", for: .normal)
                deselectButton(button: btGroup)
            }
            btGroupSelected = UIButton()
        }
    }
    
    @IBAction func updateJson(_ sender: UIBarButtonItem) {
        ws.updateWorldCupJson(completion: { success in
            if(success) {
                print("json updated")
                self.updateUI()
            }
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btGroups = [btGroupA, btGroupB, btGroupC, btGroupD, btGroupE, btGroupF, btGroupG, btGroupH]
        
        selectButton(button: btGroupsButton)
        
        updateUI()

    }
    
    func updateUI(){
        self.customActivityIndicatory(self.view, startAnimate: true)
        utils.retrieveJsonData(completion: {stadiums, tvChannels, teams, groups, knockouts in
            self.filteredGroups = groups
            self.filteredKnockouts = knockouts
            
            self.tableView.reloadData()
//            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: true)
            self.customActivityIndicatory(self.view, startAnimate: false)
            
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMatchDetails" {
            if let vc = segue.destination as? MatchDetailViewController {
                if let match = sender as? Match {
                    vc.match = match
                }
            }
        }
    }
    
    
    
    // MARK: - Functions
    
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if isGroup {
            return filteredGroups.count
        }else {
            return filteredKnockouts.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isGroup {
            return filteredGroups[section].matches.count
        } else {
            return filteredKnockouts[section].matches.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchCell") as! MatchCell
        
        var match = Match()
        
        if isGroup {
            match = filteredGroups[indexPath.section].matches[indexPath.row]
        } else {
            match = filteredKnockouts[indexPath.section].matches[indexPath.row]
        }
        
        cell.match = match
        cell.vwBackground.backgroundColor = (indexPath.row % 4) == 0 || (indexPath.row % 4) == 1 ? UIColor.white : UIColor.init(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        
        cell.lbStadium.text = utils.stadiums.first(where: {$0.id == match.stadium})?.name
        
        if match.type == "qualified" {
            cell.lbHomeTeam.text = match.home_knockout_team
            cell.lbAwayTeam.text = match.away_knockout_team
        } else {
            if let homeTeam = utils.teams.first(where: {$0.id == match.home_team}){
                cell.lbHomeTeam.text = (homeTeam.emojiString)! + " " + (homeTeam.fifaCode?.uppercased())!
            } else {
                cell.lbHomeTeam.text = "Undefined"
            }
            
            if let awayTeam = utils.teams.first(where: {$0.id == match.away_team}) {
                cell.lbAwayTeam.text = (awayTeam.fifaCode?.uppercased())! + " " + (awayTeam.emojiString)!
            } else {
                cell.lbAwayTeam.text = "Undefined"
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if isGroup {
            return filteredGroups[section].name
        } else {
            return filteredKnockouts[section].name
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if isGroup {
            performSegue(withIdentifier: "showMatchDetails", sender: filteredGroups[indexPath.section].matches[indexPath.row])
        } else {
            performSegue(withIdentifier: "showMatchDetails", sender: filteredKnockouts[indexPath.section].matches[indexPath.row])
        }
     }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        header.textLabel?.frame = header.frame
        header.textLabel?.textAlignment = .center
        header.backgroundView?.backgroundColor = UIColor.init(red: 158/255, green: 208/255, blue: 207/255, alpha: 1.0)
        
    }
}


